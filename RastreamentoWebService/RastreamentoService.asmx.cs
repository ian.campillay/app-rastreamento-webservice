using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.Services;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.Web.Mail;
using RastreamentoWebService;
using System.Net.Mail;

/// <summary>
/// faz n coisas
/// </summary>
namespace RastreamentoWebservice
{
	[ WebService(
		  Name = "RastreamentoWebservice",
		  Description = "Modulo Rastreamento",
		  Namespace = "http://localhost/RastreamentoWS/Rastreamento"
		  )]
	public class RastreamentoWebservice : System.Web.Services.WebService
	{
		 
		public RastreamentoWebservice()
		{
			try
			{		 		
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
			
		}

		#region Component Designer generated code
		
		//Required by the Web Services Designer 
		private IContainer components = null;
				
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if(disposing && components != null)
			{
				components.Dispose();
			}
			base.Dispose(disposing);		
		}
		
		#endregion
  	
		[WebMethod(Description = "Carrega dados da base da manuntecao para o pocket rastreamento.")]
		public CodBarraDataSet CarregarListaDeCodigosDeBarra()
		{
			EquipBensDataSet.EQUIPBENSDataTable EquipBensTable = new EquipBensDataSet.EQUIPBENSDataTable();

			EquipBensDataSet.EQUIPBENSRow EquipBensRow;
			
			CodBarraDataSet CodBarraDS = new CodBarraDataSet();
			
			SqlDataAdapter dataAdapter = new SqlDataAdapter();
			
			string strTableEquip, strTablePaiFilho, strConnDown, queryString, tag_estacao, cod_estacao, 
					nome_estacao, cod_man, cod_term, cod_comp, cod_conv, cod_med;
			
			char[] trimchar = new char[1];
		 	
			strConnDown= ConfigurationSettings.AppSettings["ManutencaoConnString"];
		 	
			trimchar[0] = ' ';
		 
			strTableEquip = ConfigurationSettings.AppSettings["TableEquip"];
			
			strTablePaiFilho = ConfigurationSettings.AppSettings["TablePaiFilho"];

		 	queryString = "SELECT [@T2].TZ_BEMPAI AS CODIGO_ESTACAO, " +
				"			[@T2].TZ_CODBEM AS CODIGO_EQUIPAMENTO," +
				"			[@T1].T9_CODFAMI AS CODIGO_FAMILIA, " +
				"			[@T1].T9_NOME AS NOME_EQUIPAMENTO,[@T1_1].T9_SERIE AS TAG_ESTACAO ," +
				"			[@T1_1].T9_NOME AS NOME_ESTACAO " +
				"			FROM [@T2] " +
				"			INNER JOIN [@T1] ON [@T2].TZ_CODBEM = [@T1].T9_CODBEM " +
				"			INNER JOIN [@T1] [@T1_1] ON [@T2].TZ_BEMPAI = [@T1_1].T9_CODBEM  " +
				"			WHERE ([@T2].D_E_L_E_T_ = '') " +
				"			AND ([@T1].D_E_L_E_T_ = '') " +
				"			AND ([@T2].TZ_TIPOMOV = 'E') " +
				"			ORDER BY [@T2].TZ_BEMPAI";
			
			queryString = queryString.Replace("@T1",strTableEquip);
			
			queryString = queryString.Replace("@T2",strTablePaiFilho);
	
			SqlConnection conn = new SqlConnection(strConnDown);
			
			dataAdapter.SelectCommand = new SqlCommand(queryString, conn);
			
			dataAdapter.Fill(EquipBensTable);

			EquipBensRow = (EquipBensDataSet.EQUIPBENSRow) EquipBensTable.Rows[0];			
			
			cod_estacao = EquipBensRow.CODIGO_ESTACAO.TrimEnd(trimchar);
			
			nome_estacao = EquipBensRow.NOME_ESTACAO.TrimEnd(trimchar);
			
			tag_estacao = EquipBensRow.TAG_ESTACAO.TrimEnd(trimchar);
			
			cod_man = "";
			cod_term = "";
			cod_comp = "";
			cod_conv = "";
			cod_med = "";

			
			for (int i = 0; i < EquipBensTable.Rows.Count; i++)
			{
				EquipBensRow = (EquipBensDataSet.EQUIPBENSRow) EquipBensTable.Rows[i];
				
				//verificar se trocou de estacao na lista geral puxada (equipamentos e estacoes)
				if (EquipBensRow.CODIGO_ESTACAO.TrimEnd(trimchar) != cod_estacao)
				{
					//cria uma linha nova para o equipamento e adiciona no dataset, usando os parametros anteriores
					CodBarraDS.CODIGOS_DE_BARRA.AddCODIGOS_DE_BARRARow(cod_estacao, nome_estacao, cod_man, cod_term, cod_comp, cod_conv, cod_med, tag_estacao);

					//carrega nome e codigo para a nova estacao
					cod_estacao = EquipBensRow.CODIGO_ESTACAO.TrimEnd(trimchar);
					nome_estacao = EquipBensRow.NOME_ESTACAO.TrimEnd(trimchar);
					tag_estacao = EquipBensRow.TAG_ESTACAO.TrimEnd(trimchar);

					//reseta todos os equipamentos
					cod_man = "";
					cod_term = "";
					cod_comp = "";
					cod_conv = "";
					cod_med = "";
				}

				switch (EquipBensRow.CODIGO_FAMILIA.TrimEnd(trimchar))
				{	
					case "MNS":
						cod_man = EquipBensRow.CODIGO_EQUIPAMENTO.TrimEnd(trimchar);
						break;
					case "TMS":
						cod_term = EquipBensRow.CODIGO_EQUIPAMENTO.TrimEnd(trimchar);
						break;
					case "CP":
						cod_comp = EquipBensRow.CODIGO_EQUIPAMENTO.TrimEnd(trimchar);
						break;
					case "MGAS":
						cod_med = EquipBensRow.CODIGO_EQUIPAMENTO.TrimEnd(trimchar);
						break;
					case "CV":
						cod_conv = EquipBensRow.CODIGO_EQUIPAMENTO.TrimEnd(trimchar);
						break;
					default:
						break;

				}

			}
			// o loop anterior nao processa a ultima estacao da tabela pois nao houve "troca"
			// processando ultima linha agora
			CodBarraDS.CODIGOS_DE_BARRA.AddCODIGOS_DE_BARRARow(cod_estacao, nome_estacao, cod_man, cod_term, cod_comp, cod_conv, cod_med, tag_estacao);

			return CodBarraDS;

		}
		
		 
		[WebMethod(Description = "Descarrega dados do pocket rastreamento para base de dados sgm.")]
		public void DistribuirListaDeNaoConformidades(NaoConformidadesDataSet ReceivedNCsDS)
		{
			NaoConformidadesDataSet LocalNCsDS = new NaoConformidadesDataSet();
			NaoConformidadesDataSet.NAO_CONFORMIDADESRow NCsRow;
			string strConnUp, qryNC;		
			 
			strConnUp = ConfigurationSettings.AppSettings["RastreamentoConnString"];
		 	qryNC = "SELECT * FROM NAO_CONFORMIDADES";			
			SqlConnection connNC = new SqlConnection(strConnUp);
			SqlDataAdapter daNC = new SqlDataAdapter();
			daNC.SelectCommand = new SqlCommand(qryNC, connNC);
			SqlCommandBuilder cbNC = new SqlCommandBuilder(daNC); 
			daNC.MissingSchemaAction = MissingSchemaAction.AddWithKey;
			try{
				daNC.Fill(LocalNCsDS,"NAO_CONFORMIDADES");
			}
			catch(Exception ex)
			{
				ex.ToString();
			}
			LocalNCsDS.AcceptChanges(); 
			 
			for (int i = 0; i < ReceivedNCsDS.NAO_CONFORMIDADES.Count; i++)
			{ 
				NCsRow = (NaoConformidadesDataSet.NAO_CONFORMIDADESRow) ReceivedNCsDS.NAO_CONFORMIDADES.Rows[i];
				LocalNCsDS.NAO_CONFORMIDADES.AddNAO_CONFORMIDADESRow(NCsRow.HORARIO_OCORRENCIA,NCsRow.CODIGO_ESTACAO,NCsRow.CODIGO_ESPERADO, NCsRow.CODIGO_LIDO, NCsRow.OBSERVACAO, NCsRow.CODIGO_FAMILIA,NCsRow.CONFERIDA,NCsRow.CODIGO_ISOPLAY,NCsRow.NOME_ESTACAO,NCsRow.ID_REGIAO);
 
			}
			daNC.Update(LocalNCsDS, "NAO_CONFORMIDADES");	 
			 
			//tos[id_regiao] +=  ((NaoConformidadesDataSet.EMAIL_USUARIOSRow) LocalNConfDS.EMAIL_USUARIOS.Rows[i]).EMAIL.Trim() + ";";	
			//bodys[id_regiao] +=  "<tr><td>" + NConfRow.CODIGO_ESTACAO.Trim() + "</td><td>" + NConfRow.CODIGO_ESPERADO.Trim() + "</td><td>" + NConfRow.CODIGO_LIDO.Trim() + " </td><td>" + NConfRow.CODIGO_FAMILIA.Trim() + "</td><td>" + NConfRow.HORARIO_OCORRENCIA.ToShortDateString() + "</td><td>" + NConfRow.NOME_ESTACAO.Trim() + "</td></tr>";						 
		 	
		}

		
		[WebMethod(Description = "Envia Emails de NCs")]
		public void EntregarEmail( string[] bodys )
		{		 
			string h1, h2, h3, f1;		
			string[] tos = new string[bodys.Length];
			System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
			h1 = "<HTML><P><FONT SIZE='2'>No dia " + DateTime.Now.ToShortDateString() + " foram registradas as seguintes ocorr&ecirc;ncias de n&atilde;o-conformidades pelo m&oacute;dulo de rastreamento:</P><P></P><P></P>";
			h2 = "<TABLE BORDER='1'>\n<TR>";
			h3 = "<TH>TAG da Esta&ccedil;&atilde;o</TH><TH>C&oacute;digo Esperado</TH><TH>C&oacute;digo Lido</TH><TH>N&atilde;o Conformidade / Alarme</TH><TH>Data</TH><TH>Nome da Esta&ccedil;&atilde;o</TH></TR>";	 
			f1 = "</TABLE></FONT><P><FONT color='red' size='2'>*Esta mensagem foi gerada automaticamente pelo m&oacute;dulo de rastreabilidade da manuten&ccedil;&atilde;o! <br> Favor n&atilde;o Responder este e-mail. <br> </FONT></P></HTML>";
			  
			string strConnMail, qryMail, strMailServer, strMailName, strMailFrom;	
			strMailServer = ConfigurationSettings.AppSettings["MailServer"];
			strMailName = ConfigurationSettings.AppSettings["MailName"];
			strMailFrom = ConfigurationSettings.AppSettings["MailFrom"];
			strConnMail = ConfigurationSettings.AppSettings["RastreamentoConnString"];
			 
			tos[0] = null;
			for(int i = 0; i < bodys.Length; i++)
			{
				if( bodys[i] != null && bodys[i] != "" ){
					qryMail = "SELECT ID_REGIAO, EMAIL FROM EMAIL_USUARIOS WHERE (ID_REGIAO = '"+ i + "')";
					SqlConnection connAddress = new SqlConnection(strConnMail);
					SqlDataReader sdrAddress = null;
					try
					{ 
						connAddress.Open(); 
						SqlCommand cmd = new SqlCommand(qryMail, connAddress); 
						sdrAddress = cmd.ExecuteReader(); 

						while (sdrAddress.Read())
						{
							tos[i] += sdrAddress["EMAIL"].ToString().Trim() + ";"; 
						}
						try
						{
						 
							msg.Subject = "NOTIFICA��O DE N�O-CONFORMIDADE (" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ")";						 
							msg.BodyFormat = MailFormat.Html; 
							msg.From = strMailName + " R"+ i +  "<" + strMailFrom + ">";
							msg.Priority = System.Web.Mail.MailPriority.High;
							msg.Body = h1 + h2 + h3 + bodys[i] + f1;						 
							msg.To = tos[i];						 
							SmtpMail.SmtpServer = strMailServer;
							SmtpMail.Send(msg);  
							
						}
						catch(Exception ex )
						{
							msg.Subject = "Sistemas de Rastremaento";						 
							msg.BodyFormat = MailFormat.Html; 
							msg.From = "Aviso Rastreamento" + "<" + strMailFrom + ">";						 
							msg.Priority = System.Web.Mail.MailPriority.High;
							msg.Body = tos[i];						 
							msg.To = "schmaedech@gmail.com";						 
							SmtpMail.SmtpServer = strMailServer;
							SmtpMail.Send(msg);
							while( ex.InnerException != null )
							{
								 ex = ex.InnerException;
							}
						}
					}
					finally
					{ 
						if (sdrAddress != null)
						{
							sdrAddress.Close();
						} 
						if (connAddress != null)
						{
							connAddress.Close();
						}
					}	
				
				} 
			 
				
			}	
			 	 
			 
			 
		}
	 
		
		[WebMethod(Description = "Testa conexao mail smtp")]
		public string TesteEmail(string De, string Para, string SmtpServer )
		{		 
			//string strConnMail = "Data Source=" + sDS.Settings[0].SQL_SERVER_RASTREAMENTO.Trim() + ";Initial Catalog=" + sDS.Settings[0].SQL_CATALOG_RASTREAMENTO.Trim() + ";User ID=" + sDS.Settings[0].SQL_USER_RASTREAMENTO.Trim() + ";Password=" + sDS.Settings[0].SQL_PASS_RASTREAMENTO.Trim();
			string retorno = "";
			string h1, f1;
            //string resposta = "";

            System.Web.Mail.MailMessage msg = new System.Web.Mail.MailMessage();
			h1 = "<HTML><P><FONT SIZE='2'>Teste no dia " + DateTime.Now.ToShortDateString() + "</P><P></P><P></P>";
			f1 = "*Esta mensagem foi gerada automaticamente pelo m&oacute;dulo de rastreabilidade da manuten&ccedil;&atilde;o! <br> Favor n&atilde;o Responder este e-mail. <br> </FONT></P></HTML>";
			string strConnMail, strMailServer, strMailName, strMailFrom;	
			strMailServer = ConfigurationSettings.AppSettings["MailServer"];
			strMailName = ConfigurationSettings.AppSettings["MailName"];
			strMailFrom = ConfigurationSettings.AppSettings["MailFrom"];
			strConnMail = ConfigurationSettings.AppSettings["RastreamentoConnString"];
			
			try
			{  
				 
				msg.Subject = "TESTE DE N�O-CONFORMIDADE (" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ")";
				msg.BodyFormat = MailFormat.Html;
				msg.From = "WebMethod" + "<" + De + ">";
				msg.Priority = System.Web.Mail.MailPriority.High;
				msg.Body = h1 + " "+ f1;
				msg.To = Para;				
				SmtpMail.SmtpServer = SmtpServer;
				SmtpMail.Send(msg);
				retorno = h1 + "" + f1 ;
				 
			} 			 
			catch(Exception ex )
			{
				retorno += "The following exception occurred: "  + ex.ToString() ;
				//check the InnerException
				while( ex.InnerException != null )
				{
					retorno +="--------------------------------";
					retorno +="The following InnerException reported: " + ex.InnerException.ToString() ;
					ex = ex.InnerException;
				}
			}
 
			return retorno; 
		}
	 
		
		[WebMethod(Description = "Testa conexao com base de dados sigamnt.")]
		public string TesteConnManutencao()
		{
			string retorno,strConnTesteSiga,strQryTesteSiga, strTableTesteSiga ;
			retorno = "";
			strTableTesteSiga =  ConfigurationSettings.AppSettings["TableEquip"];
		    strQryTesteSiga = "SELECT T9_CODBEM FROM " + strTableTesteSiga;
			strConnTesteSiga = ConfigurationSettings.AppSettings["ManutencaoConnString"];
			SqlConnection connTesteSiga = new SqlConnection(strConnTesteSiga);
			SqlDataReader sdrTesteSiga = null;
			try
			{ 
				connTesteSiga.Open(); 
				SqlCommand cmd = new SqlCommand(strQryTesteSiga, connTesteSiga); 
				sdrTesteSiga = cmd.ExecuteReader(); 
				while (sdrTesteSiga.Read())
				{
					 retorno += sdrTesteSiga[0];
				}
			}
			catch(Exception ex)
			{
				retorno = ex.Message;
			}
			return retorno;
		}

		
		[WebMethod(Description = "Testa conexao com base de dados sgm.")]
		public string TesteConnRastreamento()
		{
			string retorno,strConnTesteSGM,strQryTesteSGM;
			retorno = "";
			
			strQryTesteSGM = "SELECT CODIGO_ESTACAO FROM NAO_CONFORMIDADES";
			
			strConnTesteSGM = ConfigurationSettings.AppSettings["RastreamentoConnString"];
			SqlConnection connTesteSGM = new SqlConnection(strConnTesteSGM);
			SqlDataReader sdrTesteSGM = null;
			try
			{ 
				connTesteSGM.Open(); 
				SqlCommand cmd = new SqlCommand(strQryTesteSGM, connTesteSGM); 
				sdrTesteSGM = cmd.ExecuteReader(); 
				while (sdrTesteSGM.Read())
				{
					retorno += sdrTesteSGM[0];
				}
			}
			catch(Exception ex)
			{
				retorno = ex.Message;
			}
			return retorno;
		}
	
	
		[WebMethod(Description = "Carrega dados da base SGM para o pocket rastreamento.")]
		public CityGateDataSet CarregarCityGate()
		{
			
			SqlConnection sqlConn = new SqlConnection();			 
			sqlConn.ConnectionString = ConfigurationSettings.AppSettings["RastreamentoConnString"];		 
			string strQryCityGate ="SELECT * FROM sgm_CityGate";			 
			SqlDataAdapter daCityGate = new SqlDataAdapter(strQryCityGate,sqlConn);			 
			CityGateDataSet dsCityGate = new CityGateDataSet();			 
			daCityGate.Fill(dsCityGate , "sgm_CityGate");			 
			return dsCityGate;
			

		}
		
		
		[WebMethod(Description = "Carrega dados da base SGM para o pocket rastreamento.")]
		public LeituraDataSet CarregarLeitura()
		{ 
			SqlConnection sqlConn = new SqlConnection();			 
			sqlConn.ConnectionString = ConfigurationSettings.AppSettings["RastreamentoConnString"];		 
			string strQryLeitura ="SELECT * FROM sgm_Leitura WHERE (Data_Referencia >= GETDATE() - 45) AND (Data_Referencia <= GETDATE()) ";			 
			SqlDataAdapter daLeitura = new SqlDataAdapter(strQryLeitura,sqlConn);			 
			LeituraDataSet dsLeitura = new LeituraDataSet();			 
			daLeitura.Fill(dsLeitura , "sgm_Leitura");			 
			return dsLeitura; 
		}
		
		
		[WebMethod(Description = "Carrega dados da base SGM para o pocket rastreamento.")]
		public LeituristaDataSet CarregarLeiturista()
		{
			SqlConnection sqlConn = new SqlConnection();			 
			sqlConn.ConnectionString = ConfigurationSettings.AppSettings["RastreamentoConnString"];		 
			string strQryLeiturista ="SELECT * FROM sgm_Leiturista WHERE (flag_Ativo = 1)";			 
			SqlDataAdapter daLeiturista = new SqlDataAdapter(strQryLeiturista,sqlConn);			 
			LeituristaDataSet dsLeiturista = new LeituristaDataSet();			 
			daLeiturista.Fill(dsLeiturista , "sgm_Leiturista");			 
			return dsLeiturista;
		}

		
		[WebMethod(Description = "Carrega dados da base SGM para o pocket rastreamento.")]
		public PontoDeEntregaDataSet CarregarPontoDeEntrega()
		{
			SqlConnection sqlConn = new SqlConnection();			 
			sqlConn.ConnectionString = ConfigurationSettings.AppSettings["RastreamentoConnString"];		 
			string strQryPontoDeEntrega ="SELECT * FROM sgm_Ponto_Entrega";			 
			SqlDataAdapter daPontoDeEntrega = new SqlDataAdapter(strQryPontoDeEntrega,sqlConn);			 
			PontoDeEntregaDataSet dsPontoDeEntrega = new PontoDeEntregaDataSet();			 
			daPontoDeEntrega.Fill(dsPontoDeEntrega , "sgm_Ponto_Entrega");			 
			return dsPontoDeEntrega;
		}

		
		[WebMethod(Description = "Carrega dados da base SGM para o pocket rastreamento.")]
		public ConfiguracaoClienteDataSet CarregarConfiguracaoCliente()
		{
			SqlConnection sqlConn = new SqlConnection();			 
			sqlConn.ConnectionString = ConfigurationSettings.AppSettings["RastreamentoConnString"];		 
			string strQryConfiguracaoCliente ="SELECT * FROM sgm_Configuracao_Cliente";			 
			SqlDataAdapter daConfiguracaoCliente = new SqlDataAdapter(strQryConfiguracaoCliente,sqlConn);			 
			ConfiguracaoClienteDataSet dsConfiguracaoCliente = new ConfiguracaoClienteDataSet();			 
			daConfiguracaoCliente.Fill(dsConfiguracaoCliente , "sgm_Configuracao_Cliente");			 
			return dsConfiguracaoCliente;
		}
		
		
		[WebMethod(Description = "Carrega dados da base SGM para o pocket rastreamento.")]
		public FamiliaGNCDataset CarregarFamiliaGNC()
		{
			SqlConnection sqlConn = new SqlConnection();			 
			sqlConn.ConnectionString = ConfigurationSettings.AppSettings["RastreamentoConnString"];		 
			string strQryFamiliaGNC ="SELECT * FROM VOLUME_TAG_MAE_FILHO";			 
			SqlDataAdapter daFamiliaGNC = new SqlDataAdapter(strQryFamiliaGNC,sqlConn);			 
			FamiliaGNCDataset dsFamiliaGNC = new FamiliaGNCDataset();			 
			daFamiliaGNC.Fill(dsFamiliaGNC , "VOLUME_TAG_MAE_FILHO");			 
			return dsFamiliaGNC;
		}
		
		[WebMethod(Description = "Teste de conexao.")]
		public bool IsConnected()
		{ 		 
			return true;
		}
		
	}
}
