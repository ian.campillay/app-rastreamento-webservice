﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.573
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace RastreamentoWebService
{
    using System;
    using System.Data;
    using System.Xml;
    using System.Runtime.Serialization;


    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.ToolboxItem(true)]
    public class NaoConformidadesDataSet : DataSet
    {

        private NAO_CONFORMIDADESDataTable tableNAO_CONFORMIDADES;

        public NaoConformidadesDataSet()
        {
            this.InitClass();
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }

        protected NaoConformidadesDataSet(SerializationInfo info, StreamingContext context)
        {
            string strSchema = ((string)(info.GetValue("XmlSchema", typeof(string))));
            if ((strSchema != null))
            {
                DataSet ds = new DataSet();
                ds.ReadXmlSchema(new XmlTextReader(new System.IO.StringReader(strSchema)));
                if ((ds.Tables["NAO_CONFORMIDADES"] != null))
                {
                    this.Tables.Add(new NAO_CONFORMIDADESDataTable(ds.Tables["NAO_CONFORMIDADES"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else
            {
                this.InitClass();
            }
            this.GetSerializationData(info, context);
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }

        [System.ComponentModel.Browsable(false)]
        [System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public NAO_CONFORMIDADESDataTable NAO_CONFORMIDADES
        {
            get
            {
                return this.tableNAO_CONFORMIDADES;
            }
        }

        public override DataSet Clone()
        {
            NaoConformidadesDataSet cln = ((NaoConformidadesDataSet)(base.Clone()));
            cln.InitVars();
            return cln;
        }

        protected override bool ShouldSerializeTables()
        {
            return false;
        }

        protected override bool ShouldSerializeRelations()
        {
            return false;
        }

        protected override void ReadXmlSerializable(XmlReader reader)
        {
            this.Reset();
            DataSet ds = new DataSet();
            ds.ReadXml(reader);
            if ((ds.Tables["NAO_CONFORMIDADES"] != null))
            {
                this.Tables.Add(new NAO_CONFORMIDADESDataTable(ds.Tables["NAO_CONFORMIDADES"]));
            }
            this.DataSetName = ds.DataSetName;
            this.Prefix = ds.Prefix;
            this.Namespace = ds.Namespace;
            this.Locale = ds.Locale;
            this.CaseSensitive = ds.CaseSensitive;
            this.EnforceConstraints = ds.EnforceConstraints;
            this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
            this.InitVars();
        }

        protected override System.Xml.Schema.XmlSchema GetSchemaSerializable()
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            this.WriteXmlSchema(new XmlTextWriter(stream, null));
            stream.Position = 0;
            return System.Xml.Schema.XmlSchema.Read(new XmlTextReader(stream), null);
        }

        internal void InitVars()
        {
            this.tableNAO_CONFORMIDADES = ((NAO_CONFORMIDADESDataTable)(this.Tables["NAO_CONFORMIDADES"]));
            if ((this.tableNAO_CONFORMIDADES != null))
            {
                this.tableNAO_CONFORMIDADES.InitVars();
            }
        }

        private void InitClass()
        {
            this.DataSetName = "NaoConformidadesDataSet";
            this.Prefix = "";
            this.Namespace = "http://tempuri.org/NaoConformidadesDataSet.xsd";
            this.Locale = new System.Globalization.CultureInfo("en-US");
            this.CaseSensitive = false;
            this.EnforceConstraints = true;
            this.tableNAO_CONFORMIDADES = new NAO_CONFORMIDADESDataTable();
            this.Tables.Add(this.tableNAO_CONFORMIDADES);
        }

        private bool ShouldSerializeNAO_CONFORMIDADES()
        {
            return false;
        }

        private void SchemaChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e)
        {
            if ((e.Action == System.ComponentModel.CollectionChangeAction.Remove))
            {
                this.InitVars();
            }
        }

        public delegate void NAO_CONFORMIDADESRowChangeEventHandler(object sender, NAO_CONFORMIDADESRowChangeEvent e);

        [System.Diagnostics.DebuggerStepThrough()]
        public class NAO_CONFORMIDADESDataTable : DataTable, System.Collections.IEnumerable
        {

            private DataColumn columnID_NAO_CONFORMIDADE;

            private DataColumn columnHORARIO_OCORRENCIA;

            private DataColumn columnCODIGO_ESTACAO;

            private DataColumn columnCODIGO_ESPERADO;

            private DataColumn columnCODIGO_LIDO;

            private DataColumn columnOBSERVACAO;

            private DataColumn columnCODIGO_FAMILIA;

            private DataColumn columnCONFERIDA;

            private DataColumn columnCODIGO_ISOPLAY;

            private DataColumn columnNOME_ESTACAO;

            private DataColumn columnID_REGIAO;

            internal NAO_CONFORMIDADESDataTable() :
                    base("NAO_CONFORMIDADES")
            {
                this.InitClass();
            }

            internal NAO_CONFORMIDADESDataTable(DataTable table) :
                    base(table.TableName)
            {
                if ((table.CaseSensitive != table.DataSet.CaseSensitive))
                {
                    this.CaseSensitive = table.CaseSensitive;
                }
                if ((table.Locale.ToString() != table.DataSet.Locale.ToString()))
                {
                    this.Locale = table.Locale;
                }
                if ((table.Namespace != table.DataSet.Namespace))
                {
                    this.Namespace = table.Namespace;
                }
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
                this.DisplayExpression = table.DisplayExpression;
            }

            [System.ComponentModel.Browsable(false)]
            public int Count
            {
                get
                {
                    return this.Rows.Count;
                }
            }

            internal DataColumn ID_NAO_CONFORMIDADEColumn
            {
                get
                {
                    return this.columnID_NAO_CONFORMIDADE;
                }
            }

            internal DataColumn HORARIO_OCORRENCIAColumn
            {
                get
                {
                    return this.columnHORARIO_OCORRENCIA;
                }
            }

            internal DataColumn CODIGO_ESTACAOColumn
            {
                get
                {
                    return this.columnCODIGO_ESTACAO;
                }
            }

            internal DataColumn CODIGO_ESPERADOColumn
            {
                get
                {
                    return this.columnCODIGO_ESPERADO;
                }
            }

            internal DataColumn CODIGO_LIDOColumn
            {
                get
                {
                    return this.columnCODIGO_LIDO;
                }
            }

            internal DataColumn OBSERVACAOColumn
            {
                get
                {
                    return this.columnOBSERVACAO;
                }
            }

            internal DataColumn CODIGO_FAMILIAColumn
            {
                get
                {
                    return this.columnCODIGO_FAMILIA;
                }
            }

            internal DataColumn CONFERIDAColumn
            {
                get
                {
                    return this.columnCONFERIDA;
                }
            }

            internal DataColumn CODIGO_ISOPLAYColumn
            {
                get
                {
                    return this.columnCODIGO_ISOPLAY;
                }
            }

            internal DataColumn NOME_ESTACAOColumn
            {
                get
                {
                    return this.columnNOME_ESTACAO;
                }
            }

            internal DataColumn ID_REGIAOColumn
            {
                get
                {
                    return this.columnID_REGIAO;
                }
            }

            public NAO_CONFORMIDADESRow this[int index]
            {
                get
                {
                    return ((NAO_CONFORMIDADESRow)(this.Rows[index]));
                }
            }

            public event NAO_CONFORMIDADESRowChangeEventHandler NAO_CONFORMIDADESRowChanged;

            public event NAO_CONFORMIDADESRowChangeEventHandler NAO_CONFORMIDADESRowChanging;

            public event NAO_CONFORMIDADESRowChangeEventHandler NAO_CONFORMIDADESRowDeleted;

            public event NAO_CONFORMIDADESRowChangeEventHandler NAO_CONFORMIDADESRowDeleting;

            public void AddNAO_CONFORMIDADESRow(NAO_CONFORMIDADESRow row)
            {
                this.Rows.Add(row);
            }

            public NAO_CONFORMIDADESRow AddNAO_CONFORMIDADESRow(System.DateTime HORARIO_OCORRENCIA, string CODIGO_ESTACAO, string CODIGO_ESPERADO, string CODIGO_LIDO, string OBSERVACAO, string CODIGO_FAMILIA, string CONFERIDA, string CODIGO_ISOPLAY, string NOME_ESTACAO, string ID_REGIAO)
            {
                NAO_CONFORMIDADESRow rowNAO_CONFORMIDADESRow = ((NAO_CONFORMIDADESRow)(this.NewRow()));
                rowNAO_CONFORMIDADESRow.ItemArray = new object[] {
                        null,
                        HORARIO_OCORRENCIA,
                        CODIGO_ESTACAO,
                        CODIGO_ESPERADO,
                        CODIGO_LIDO,
                        OBSERVACAO,
                        CODIGO_FAMILIA,
                        CONFERIDA,
                        CODIGO_ISOPLAY,
                        NOME_ESTACAO,
                        ID_REGIAO};
                this.Rows.Add(rowNAO_CONFORMIDADESRow);
                return rowNAO_CONFORMIDADESRow;
            }

            public NAO_CONFORMIDADESRow FindByID_NAO_CONFORMIDADE(int ID_NAO_CONFORMIDADE)
            {
                return ((NAO_CONFORMIDADESRow)(this.Rows.Find(new object[] {
                            ID_NAO_CONFORMIDADE})));
            }

            public System.Collections.IEnumerator GetEnumerator()
            {
                return this.Rows.GetEnumerator();
            }

            public override DataTable Clone()
            {
                NAO_CONFORMIDADESDataTable cln = ((NAO_CONFORMIDADESDataTable)(base.Clone()));
                cln.InitVars();
                return cln;
            }

            protected override DataTable CreateInstance()
            {
                return new NAO_CONFORMIDADESDataTable();
            }

            internal void InitVars()
            {
                this.columnID_NAO_CONFORMIDADE = this.Columns["ID_NAO_CONFORMIDADE"];
                this.columnHORARIO_OCORRENCIA = this.Columns["HORARIO_OCORRENCIA"];
                this.columnCODIGO_ESTACAO = this.Columns["CODIGO_ESTACAO"];
                this.columnCODIGO_ESPERADO = this.Columns["CODIGO_ESPERADO"];
                this.columnCODIGO_LIDO = this.Columns["CODIGO_LIDO"];
                this.columnOBSERVACAO = this.Columns["OBSERVACAO"];
                this.columnCODIGO_FAMILIA = this.Columns["CODIGO_FAMILIA"];
                this.columnCONFERIDA = this.Columns["CONFERIDA"];
                this.columnCODIGO_ISOPLAY = this.Columns["CODIGO_ISOPLAY"];
                this.columnNOME_ESTACAO = this.Columns["NOME_ESTACAO"];
                this.columnID_REGIAO = this.Columns["ID_REGIAO"];
            }

            private void InitClass()
            {
                this.columnID_NAO_CONFORMIDADE = new DataColumn("ID_NAO_CONFORMIDADE", typeof(int), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnID_NAO_CONFORMIDADE);
                this.columnHORARIO_OCORRENCIA = new DataColumn("HORARIO_OCORRENCIA", typeof(System.DateTime), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnHORARIO_OCORRENCIA);
                this.columnCODIGO_ESTACAO = new DataColumn("CODIGO_ESTACAO", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnCODIGO_ESTACAO);
                this.columnCODIGO_ESPERADO = new DataColumn("CODIGO_ESPERADO", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnCODIGO_ESPERADO);
                this.columnCODIGO_LIDO = new DataColumn("CODIGO_LIDO", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnCODIGO_LIDO);
                this.columnOBSERVACAO = new DataColumn("OBSERVACAO", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnOBSERVACAO);
                this.columnCODIGO_FAMILIA = new DataColumn("CODIGO_FAMILIA", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnCODIGO_FAMILIA);
                this.columnCONFERIDA = new DataColumn("CONFERIDA", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnCONFERIDA);
                this.columnCODIGO_ISOPLAY = new DataColumn("CODIGO_ISOPLAY", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnCODIGO_ISOPLAY);
                this.columnNOME_ESTACAO = new DataColumn("NOME_ESTACAO", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnNOME_ESTACAO);
                this.columnID_REGIAO = new DataColumn("ID_REGIAO", typeof(string), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnID_REGIAO);
                this.Constraints.Add(new UniqueConstraint("NaoConformidadesDataSetKey1", new DataColumn[] {
                                this.columnID_NAO_CONFORMIDADE}, true));
                this.columnID_NAO_CONFORMIDADE.AutoIncrement = true;
                this.columnID_NAO_CONFORMIDADE.AllowDBNull = false;
                this.columnID_NAO_CONFORMIDADE.ReadOnly = true;
                this.columnID_NAO_CONFORMIDADE.Unique = true;
            }

            public NAO_CONFORMIDADESRow NewNAO_CONFORMIDADESRow()
            {
                return ((NAO_CONFORMIDADESRow)(this.NewRow()));
            }

            protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
            {
                return new NAO_CONFORMIDADESRow(builder);
            }

            protected override System.Type GetRowType()
            {
                return typeof(NAO_CONFORMIDADESRow);
            }

            protected override void OnRowChanged(DataRowChangeEventArgs e)
            {
                base.OnRowChanged(e);
                if ((this.NAO_CONFORMIDADESRowChanged != null))
                {
                    this.NAO_CONFORMIDADESRowChanged(this, new NAO_CONFORMIDADESRowChangeEvent(((NAO_CONFORMIDADESRow)(e.Row)), e.Action));
                }
            }

            protected override void OnRowChanging(DataRowChangeEventArgs e)
            {
                base.OnRowChanging(e);
                if ((this.NAO_CONFORMIDADESRowChanging != null))
                {
                    this.NAO_CONFORMIDADESRowChanging(this, new NAO_CONFORMIDADESRowChangeEvent(((NAO_CONFORMIDADESRow)(e.Row)), e.Action));
                }
            }

            protected override void OnRowDeleted(DataRowChangeEventArgs e)
            {
                base.OnRowDeleted(e);
                if ((this.NAO_CONFORMIDADESRowDeleted != null))
                {
                    this.NAO_CONFORMIDADESRowDeleted(this, new NAO_CONFORMIDADESRowChangeEvent(((NAO_CONFORMIDADESRow)(e.Row)), e.Action));
                }
            }

            protected override void OnRowDeleting(DataRowChangeEventArgs e)
            {
                base.OnRowDeleting(e);
                if ((this.NAO_CONFORMIDADESRowDeleting != null))
                {
                    this.NAO_CONFORMIDADESRowDeleting(this, new NAO_CONFORMIDADESRowChangeEvent(((NAO_CONFORMIDADESRow)(e.Row)), e.Action));
                }
            }

            public void RemoveNAO_CONFORMIDADESRow(NAO_CONFORMIDADESRow row)
            {
                this.Rows.Remove(row);
            }
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public class NAO_CONFORMIDADESRow : DataRow
        {

            private NAO_CONFORMIDADESDataTable tableNAO_CONFORMIDADES;

            internal NAO_CONFORMIDADESRow(DataRowBuilder rb) :
                    base(rb)
            {
                this.tableNAO_CONFORMIDADES = ((NAO_CONFORMIDADESDataTable)(this.Table));
            }

            public int ID_NAO_CONFORMIDADE
            {
                get
                {
                    return ((int)(this[this.tableNAO_CONFORMIDADES.ID_NAO_CONFORMIDADEColumn]));
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.ID_NAO_CONFORMIDADEColumn] = value;
                }
            }

            public System.DateTime HORARIO_OCORRENCIA
            {
                get
                {
                    try
                    {
                        return ((System.DateTime)(this[this.tableNAO_CONFORMIDADES.HORARIO_OCORRENCIAColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.HORARIO_OCORRENCIAColumn] = value;
                }
            }

            public string CODIGO_ESTACAO
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.CODIGO_ESTACAOColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.CODIGO_ESTACAOColumn] = value;
                }
            }

            public string CODIGO_ESPERADO
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.CODIGO_ESPERADOColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.CODIGO_ESPERADOColumn] = value;
                }
            }

            public string CODIGO_LIDO
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.CODIGO_LIDOColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.CODIGO_LIDOColumn] = value;
                }
            }

            public string OBSERVACAO
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.OBSERVACAOColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.OBSERVACAOColumn] = value;
                }
            }

            public string CODIGO_FAMILIA
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.CODIGO_FAMILIAColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.CODIGO_FAMILIAColumn] = value;
                }
            }

            public string CONFERIDA
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.CONFERIDAColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.CONFERIDAColumn] = value;
                }
            }

            public string CODIGO_ISOPLAY
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.CODIGO_ISOPLAYColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.CODIGO_ISOPLAYColumn] = value;
                }
            }

            public string NOME_ESTACAO
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.NOME_ESTACAOColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.NOME_ESTACAOColumn] = value;
                }
            }

            public string ID_REGIAO
            {
                get
                {
                    try
                    {
                        return ((string)(this[this.tableNAO_CONFORMIDADES.ID_REGIAOColumn]));
                    }
                    catch (InvalidCastException e)
                    {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set
                {
                    this[this.tableNAO_CONFORMIDADES.ID_REGIAOColumn] = value;
                }
            }

            public bool IsHORARIO_OCORRENCIANull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.HORARIO_OCORRENCIAColumn);
            }

            public void SetHORARIO_OCORRENCIANull()
            {
                this[this.tableNAO_CONFORMIDADES.HORARIO_OCORRENCIAColumn] = System.Convert.DBNull;
            }

            public bool IsCODIGO_ESTACAONull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.CODIGO_ESTACAOColumn);
            }

            public void SetCODIGO_ESTACAONull()
            {
                this[this.tableNAO_CONFORMIDADES.CODIGO_ESTACAOColumn] = System.Convert.DBNull;
            }

            public bool IsCODIGO_ESPERADONull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.CODIGO_ESPERADOColumn);
            }

            public void SetCODIGO_ESPERADONull()
            {
                this[this.tableNAO_CONFORMIDADES.CODIGO_ESPERADOColumn] = System.Convert.DBNull;
            }

            public bool IsCODIGO_LIDONull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.CODIGO_LIDOColumn);
            }

            public void SetCODIGO_LIDONull()
            {
                this[this.tableNAO_CONFORMIDADES.CODIGO_LIDOColumn] = System.Convert.DBNull;
            }

            public bool IsOBSERVACAONull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.OBSERVACAOColumn);
            }

            public void SetOBSERVACAONull()
            {
                this[this.tableNAO_CONFORMIDADES.OBSERVACAOColumn] = System.Convert.DBNull;
            }

            public bool IsCODIGO_FAMILIANull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.CODIGO_FAMILIAColumn);
            }

            public void SetCODIGO_FAMILIANull()
            {
                this[this.tableNAO_CONFORMIDADES.CODIGO_FAMILIAColumn] = System.Convert.DBNull;
            }

            public bool IsCONFERIDANull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.CONFERIDAColumn);
            }

            public void SetCONFERIDANull()
            {
                this[this.tableNAO_CONFORMIDADES.CONFERIDAColumn] = System.Convert.DBNull;
            }

            public bool IsCODIGO_ISOPLAYNull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.CODIGO_ISOPLAYColumn);
            }

            public void SetCODIGO_ISOPLAYNull()
            {
                this[this.tableNAO_CONFORMIDADES.CODIGO_ISOPLAYColumn] = System.Convert.DBNull;
            }

            public bool IsNOME_ESTACAONull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.NOME_ESTACAOColumn);
            }

            public void SetNOME_ESTACAONull()
            {
                this[this.tableNAO_CONFORMIDADES.NOME_ESTACAOColumn] = System.Convert.DBNull;
            }

            public bool IsID_REGIAONull()
            {
                return this.IsNull(this.tableNAO_CONFORMIDADES.ID_REGIAOColumn);
            }

            public void SetID_REGIAONull()
            {
                this[this.tableNAO_CONFORMIDADES.ID_REGIAOColumn] = System.Convert.DBNull;
            }
        }

        [System.Diagnostics.DebuggerStepThrough()]
        public class NAO_CONFORMIDADESRowChangeEvent : EventArgs
        {

            private NAO_CONFORMIDADESRow eventRow;

            private DataRowAction eventAction;

            public NAO_CONFORMIDADESRowChangeEvent(NAO_CONFORMIDADESRow row, DataRowAction action)
            {
                this.eventRow = row;
                this.eventAction = action;
            }

            public NAO_CONFORMIDADESRow Row
            {
                get
                {
                    return this.eventRow;
                }
            }

            public DataRowAction Action
            {
                get
                {
                    return this.eventAction;
                }
            }
        }
    }
}
